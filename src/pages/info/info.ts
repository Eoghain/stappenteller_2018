import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'info.html'
})
export class infoPage {

  constructor(public navCtrl: NavController) {

  }

  goBack() {
    this.navCtrl.pop();
  }

}
